package main;
import model.Calculator;

import java.util.Scanner;

public class Main {
    public static void printOperation(){
        System.out.println("Choose: ");
        System.out.println("+ : add");
        System.out.println("- : subtract");
        System.out.println("* : multiply");
        System.out.println("/ : divide");
        System.out.println("C : clear");
        System.out.println("x : exit");
    }

    public static void main(String[] args) {

        boolean isOn = true;
        double result = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("enter a number: ");
        String userInput = input.nextLine();
        printOperation();
        while(isOn){
            String operation = input.nextLine();
            if(operation.equals("x"))
                break;
            if(operation.equals("C") || operation.equals("c")){

                System.out.println("enter number: ");
                userInput = input.nextLine();
                printOperation();
                operation = input.nextLine();


            }
            System.out.println("give a number: ");
            String userInputSecond = input.nextLine();
            switch (operation){
                case "+":
                    result = Calculator.addition(Double.parseDouble(userInput), Double.parseDouble(userInputSecond));
                    break;
                case "-":
                    result = Calculator.subtraction(Double.parseDouble(userInput), Double.parseDouble(userInputSecond));
                    break;
                case "*":
                    result = Calculator.multiplication(Double.parseDouble(userInput), Double.parseDouble(userInputSecond));
                    break;
                case "/":
                    result = Calculator.division(Double.parseDouble(userInput), Double.parseDouble(userInputSecond));
                    break;
                default:
                    result = 0;
                    System.out.println("invalid operation!");
            }
            System.out.println("result = " + result);
            userInput = String.valueOf(result);
            printOperation();
        }
    }
}